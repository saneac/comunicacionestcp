package com.ks.pruebas;


import com.ks.licenciamiento.Licencia;
import com.ks.pruebas.comunicaciones.Conexion;
import com.ks.tcp.Cliente;
import com.ks.tcp.Servidor;
import com.ks.tcp.Tcp;

public class App
{
    public static void main(String[] args)
    {
        Licencia.setConfiguracion("ATM");
        Licencia.setComponente("KM Replicador");
        if (Licencia.validar())
        {
            Tcp VLobjConexion;
            if (args[0].toString().toUpperCase().equals("SERVIDOR"))
            {
                VLobjConexion = new Servidor();
            }
            else
            {
                VLobjConexion = new Cliente();
            }
            Conexion miClase = new Conexion();
            VLobjConexion.setEventos(miClase);
            VLobjConexion.setPuerto(5000);
            VLobjConexion.setIP("localhost");
            try
            {
                VLobjConexion.conectar();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
