package com.ks.pruebas.comunicaciones;

import com.ks.licenciamiento.Licencia;
import com.ks.tcp.Cliente;
import com.ks.tcp.EventosTCP;
import com.ks.tcp.Tcp;

/**
 * Created by migue on 29/12/2016.
 */
public class Conexion implements EventosTCP
{
    public void conexionEstablecida(Cliente cliente)
    {
        System.out.println("Conexion establecida");
    }

    public void errorConexion(String s)
    {
        System.out.println("Error de conexion");
    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        if (Licencia.isActivo())
        {
            System.out.println("Mensaje: " + s + " - " + tcp.toString());
        }
        else
        {
            System.exit(0);
        }
    }

    public void cerrarConexion(Cliente cliente)
    {
        System.out.println("Cerrando conexion");
    }
}
